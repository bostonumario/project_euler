"""
Project Euler Problem 7

By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.

What is the 10 001st prime number?
"""

import time

def prime(number):
    """
    Looks primarily for odd factors and eliminates them.

    The following:
    number**0.5+1

    Was taken from http://code.jasonbhill.com/python/project-euler-problem-7/

    """
    divisor = 3
    while divisor < number**0.5+1:
        if number % divisor == 0:
            return False
        divisor += 2
    return True


def find_prime(number):
    """
    Takes number as input this will serve as the final point for the counter.  Loops through and tries to find prime
    numbers.  Counter starts with Prime number 2 as the first prime number. Increments from then on using odd numbers
    """
    prime_number=2
    prime_counter=1
    # While loop that uses a counter until it reaches the value of number(Nth prime)
    while prime_counter < number:
        # We already know that 2 is prime.  This logic states that if the value is greater than 2 increment by two i.e.
        # 3, 5, etc.
        # Included this since I wanted find_prime to be flexible enough for anyone to use it to find any Nth prime
        # including the 1st.
        if prime_number > 2:
            prime_number += 2
        else:
            prime_number += 1
        # Makes sure that only if a number is prime do we increment the prime_counter
        if prime(prime_number):
            prime_counter += 1
    print prime_number


start = time.time()
find_prime(10001)
elapsed = (time.time() - start)

print "Execution time %s seconds" % (elapsed)



