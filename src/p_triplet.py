"""
Project Euler Problem 9

A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,

a2 + b2 = c2
For example, 32 + 42 = 9 + 16 = 25 = 52.

There exists exactly one Pythagorean triplet for which a + b + c = 1000.
Find the product abc.
"""
import random
import time

def pyth_triplet(m, n):
    """
    The following uses Euclid's formula in order to generate pythagorean triplets.
    It takes integers m and n as long as m is greater than n.

    http://en.wikipedia.org/wiki/Pythagorean_triple
    """
    a = m**2 - n**2
    b = 2 * m * n
    c = m**2 + n**2
    return [a,b,c]

def find_1000():
    """
    This function utilizes pyth_triplet in order to generate a list of triplets to find the one triplet that's sum equals
    to 1000.
    """
    the_sum = 0
    triplet = []

    # Loop keeps running until 1000 is found
    while the_sum != 1000:
        # Decided to try something different and instead of iterating through a large grid of numbers I used a random
        # number from a range.  This could be modified if you need to find a larger number.
        m = random.randrange(1, 30)
        n = random.randrange(1, 30)
        # If n is greater than m, restart the loop again. Otherwise generate a triplet and sum the values together
        if n > m:
            continue
        else:
            triplet = pyth_triplet(m, n)
            the_sum = reduce(lambda x, y: x+y, triplet)


    the_product = reduce(lambda x, y: x*y, triplet)
    print "The triplet who's sum equals 1000 is %s" % (triplet)
    print "The product of %s is %s" % (triplet, the_product)

start = time.time()
find_1000()
elapsed = (time.time() - start)
print "Execution time %s seconds" % (elapsed)