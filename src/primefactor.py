"""
Project Euler Problem 3

The prime factors of 13195 are 5, 7, 13 and 29.

What is the largest prime factor of the number 600851475143 ?
"""
import time

def prime_factors(number):
    """
    The following function takes a number and finds the largest prime factor for that number.

    The algorithm for this was described in the following page:

    math.stackexchange.com/questions/389675/largest-prime-factor-of-60081475143

    """
    # Starts with the prime number 2 and declares the largest store
    divisor = 2
    largest = 0
    while number > 1:
        # Divides the number by the divisor, stores the quotient as the new number. Sets largest to the value of the
        # divisor. Resets divisor to 2
        if number % divisor == 0:
            number = number / divisor
            largest = divisor
            divisor = 2

        # If the number is not able to be divided by the current divisor with no remainder, the loop increments the
        # divisor

        divisor += 1
    print largest

start = time.time()
prime_factors(600851475143)

elapsed = (time.time() - start)

print "Execution time %s seconds" % (elapsed)

