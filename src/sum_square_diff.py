"""
Project Euler Problem 6

The sum of the squares of the first ten natural numbers is,

12 + 22 + ... + 102 = 385
The square of the sum of the first ten natural numbers is,

(1 + 2 + ... + 10)2 = 552 = 3025
Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is
3025 - 385 = 2640.
Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.
"""
import time

def sum_square_diff(number):
    """
    Takes in a number to set the range, calculates the sum of the squares of all the numbers in range. Sums all the
    numbers in the range and then squares the sum.  Prints out the difference between the two.

    """
    squarethesum = 0
    sumofsquares = 0
    for i in range(1, number + 1):
        sumofsquares += i**2
        squarethesum += i
    squarethesum = squarethesum**2

    print squarethesum - sumofsquares

start = time.time()
sum_square_diff(100)
elapsed = (time.time() - start)

print "Execution time %s seconds" % (elapsed)




