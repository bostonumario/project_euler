"""
Project Euler Problem 5

2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.

What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?


"""
import time
from fractions import gcd

def lcm(numbers):
    """
    The solution to this one was simple using the built in GCD function in fractions and defining a function for
    least common denominator. Found in several places but the following was the simplest form.

    http://www.enrico-franchi.org/2010/09/nice-functional-lcm-in-python.html
    """
    return reduce(lambda x, y: (x*y) // gcd(x,y), numbers, 1)

# Passes a range of values to the function.

start = time.time()
print lcm(range(1,20))
elapsed = (time.time() - start)

print "Execution time %s seconds" % (elapsed)