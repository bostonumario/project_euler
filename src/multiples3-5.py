"""
Project Euler Problem 1

If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these
multiples is 23.

Find the sum of all the multiples of 3 or 5 below 1000.
"""
import time
start = time.time()
multiple = 1
# Two lists one for multiple of 3 and the other for multiple of 5
multipleOf3 = []
multipleOf5 = []


# Iterates through numbers starting from 1 to less than 1000. If the number divided by 3 or 5 has no remainder it is
# added to the respective list.
while multiple < 1000:
    if multiple % 3 == 0:
        multipleOf3.append(multiple)
    if multiple % 5 == 0:
        multipleOf5.append(multiple)
    multiple += 1

# Takes the two lists created in the loop above and creates a set from their union.  Only unique entries are stored.
set_of_multiples = set().union(multipleOf3, multipleOf5)



sum = 0
# Iterates through the set and adds the numbers to the sum.
for number in set_of_multiples:
    sum = sum + number

print sum
elapsed = (time.time() - start)

print "Execution time %s seconds" % (elapsed)