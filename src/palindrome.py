"""
Project Euler Problem 4

A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is

9009 = 91 x 99.

Find the largest palindrome made from the product of two 3-digit numbers.
"""

import time

def ispalindrome(number):
    """
    Takes one string and reverses it.  It is then compared to see if they are the same.
    """
    main = str(number)
    # Since strings are lists [::-1] simply reverses the order of the characters
    reverse = main[::-1]
    if main == reverse:
        return True
    return False

def largest_palindrome():
    """
    Counts down from the largest of the 3 digit numbers and compares the output to see if it is a palindrome.
    Places the correct results into a dictionary.
    """
    num_dictionary = {}
    for i in range(999,99, -1):
        for j in range(999,99, -1):
            if ispalindrome(i * j):
                key = str(i) + " * " + str(j)
                num_dictionary[key] = i * j
    # Returns the result of the largest value in the dictionary
    largest = max(num_dictionary.values())

    # The following creates a list of values and returns the position of the value that matches largest.
    # obtained from stackoverflow.com/questions/8023306/get-key-by-value-in-dictionary
    print num_dictionary.keys()[num_dictionary.values().index(largest)] + " = " +str(largest)

start = time.time()
largest_palindrome()
elapsed = (time.time() - start)

print "Execution time %s seconds" % (elapsed)

